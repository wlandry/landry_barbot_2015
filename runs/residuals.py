import matplotlib.pyplot as plot

plot.xlabel('Number of V-cycles')
plot.ylabel('residual')
plot.semilogy()

Tohoku=loadtxt("Tohoku/Tohoku_residuals")
plot.plot(Tohoku[:,0],Tohoku[:,1],'-+',markersize=10)

Landers=loadtxt("Landers/Landers_residuals")
plot.plot(Landers[:,0],Landers[:,1],'-o',markersize=10)

ElMayor=loadtxt("ElMayor/ElMayor_residuals")
plot.plot(ElMayor[:,0],ElMayor[:,1],'-D',markersize=10)



plot.legend(['Tohoku','Landers','El Mayor'])

plot.show()
