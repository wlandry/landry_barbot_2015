import matplotlib.pyplot as plt

dvx_5=loadtxt("5.txt")
xyz_5=0.2*(arange(0,len(dvx_5[0]))-12)-.2

dvx_6=loadtxt("6.txt")
xyz_6=(0.2/2)*(arange(0,len(dvx_6[0]))-30)-.2

dvx_7=loadtxt("7.txt")
xyz_7=(0.2/4)*(arange(0,len(dvx_7[0]))-38)-.2

dvx_8=loadtxt("8.txt")
xyz_8=(0.2/8)*(arange(0,len(dvx_8[0]))-72)-.2

dvx_9=loadtxt("9.txt")
xyz_9=(0.2/16)*(arange(0,len(dvx_9[0]))-57)-.2

ratio_56=(dvx_5[1]+dvx_5[2])[:27]/(dvx_6[3]+dvx_6[4])[6::2]
ratio_67=(dvx_6[3]+dvx_6[4])[11:41]/(dvx_7[7]+dvx_7[8])[::2]
ratio_78=(dvx_7[7]+dvx_7[8])[2:50]/(dvx_8[15]+dvx_8[16])[::2]
ratio_89=(dvx_8[15]+dvx_8[16])[54:94]/(dvx_9[31]+dvx_9[32])[1::2]

plt.xticks(array([-.24,-.2,-.16]),fontsize=20)
plt.yticks(arange(-6,4,2),fontsize=20)

plt.ylabel('$\delta v_x$',fontsize=30)
plt.xlabel('$x$',fontsize=30)

plt.xlim(-0.26,-.14)
plt.ylim(-7,2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=20)

plt.plot(xyz_5,dvx_5[1]+dvx_5[2],'--D')
plt.plot(xyz_6,dvx_6[3]+dvx_6[4],'--D')
plt.plot(xyz_7,dvx_7[7]+dvx_7[8],'--D')
plt.plot(xyz_8,dvx_8[15]+dvx_8[16],'--D')
plt.plot(xyz_9,dvx_9[31]+dvx_9[32],'--D')

plt.plot(xyz_5[:27],ratio_56,'--D')
plt.plot(xyz_6[11:41],ratio_67,'--D')
plt.plot(xyz_7[2:50],ratio_78,'--D')
plt.plot(xyz_8[54:94],ratio_89,'--D')


plt.legend(('$32^3$', '$64^3$', '$128^3$'),loc=2)
plt.show()

