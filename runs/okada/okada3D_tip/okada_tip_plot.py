import matplotlib.pyplot as plt

low=loadtxt("okada_tip_low_vx.txt")
xyz_low=0.05*(arange(0,len(low))-38)-.2

medium=loadtxt("okada_tip_medium_vx.txt")
xyz_medium=(0.05/2)*(arange(0,len(medium))-72)-.2

high=loadtxt("okada_tip_high_vx.txt")
xyz_high=(0.05/4)*(arange(0,len(high))-84)-.2

okada=loadtxt("okada_tip_high_okada_vx.txt")
xyz_okada=(0.05/4)*(arange(0,len(okada))-84)-.2

plt.xticks(array([-.24,-.2,-.16]),fontsize=20)
plt.yticks(arange(-6,4,2),fontsize=20)

plt.ylabel('$v_x$',fontsize=30)
plt.xlabel('$x$',fontsize=30)

plt.xlim(-0.26,-.14)
plt.ylim(-7,2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=20)
plt.plot(xyz_low,low,'--D',zorder=4)
plt.plot(xyz_medium,medium,'-.^',zorder=3)
plt.plot(xyz_high,high,':s',zorder=2)
plt.plot(xyz_okada,okada,'-o',zorder=1)
plt.legend(('$32^3$', '$64^3$', '$128^3$','Okada'),loc=2)
plt.show()

