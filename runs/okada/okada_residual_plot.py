import matplotlib.pyplot as plot
import numpy

resid2D=numpy.loadtxt("okada_residual")/40
plot.ylabel('$r / ( \mu s/L^2 )$')
plot.xlabel('Number of V-cycles')
plot.semilogy()

resid3D=numpy.loadtxt("okada3D_residual")/40

plot.plot(resid2D,'-+',markersize=10)
plot.plot(resid3D,'-o',markersize=10)
plot.legend(['Okada 2D','Okada 3D'])

# tenth_range=range(0,6)
# tenth=[]
# for l in tenth_range:
#     tenth.append(1000*0.1**l)

# quarter_range=range(0,12)
# quarter=[]
# for l in quarter_range:
#     quarter.append(100000*0.25**l)

# plot.plot(resid2D,'-+',markersize=10)
# plot.plot(quarter_range,quarter,'--D')
# plot.plot(resid3D,'-x',markersize=10)
# plot.plot(tenth_range,tenth,'--o')

# plot.legend(['Okada 2D','1/4','Okada 3D','1/10'])

plot.show()
