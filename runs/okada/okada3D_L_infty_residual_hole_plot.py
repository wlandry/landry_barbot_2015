import matplotlib.pyplot as plot
import numpy

residual=numpy.loadtxt("okada3D_L_infty_residual_hole")/10
level=numpy.arange(len(residual))
h=0.4*pow(2.0,-level)

plot.figure(figsize=(12,9))
plot.loglog()
plot.xlabel('$h$',fontsize=30)
plot.xlim(6e-4,0.5)
plot.ylim(0.0001,0.15)

plot.rc('text', usetex=True)
plot.rc('font', family='serif', size=20)
plot.plot(h,residual,'-D',linewidth=4)
plot.plot(h,2*h/10,'-.',linewidth=4)
plot.legend(['$L^{\infty}$ error/slip','$O(h)$'],loc=2)

plot.show()
