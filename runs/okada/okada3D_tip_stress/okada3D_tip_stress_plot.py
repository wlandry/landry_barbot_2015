#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy

dx=7.8125e-3
low=numpy.loadtxt("okada3D_level_7_stress_xx.txt")/20
xyz_low=dx*(numpy.arange(0,len(low))-37.5)-.2

medium=numpy.loadtxt("okada3D_level_8_stress_xx.txt")/20
xyz_medium=(dx/2)*(numpy.arange(0,len(medium))-71.5)-.2

high=numpy.loadtxt("okada3D_level_9_stress_xx.txt")/20
xyz_high=(dx/4)*(numpy.arange(0,len(high))-83.5)-.2

okada=numpy.loadtxt("okada3D_okada_level_9_stress_xx.txt")/20
xyz_okada=(dx/4)*(numpy.arange(0,len(okada))-83.5)-.2

plt.figure(figsize=(12,9))
# plt.yticks(numpy.arange(-6,4,2),fontsize=20)

plt.ylabel('$\sigma_{xx}/(\mu s/L)$',fontsize=30)
plt.xlabel('$x$',fontsize=30)

plt.xlim(-0.2 - 6*dx,-.2 + 6*dx)
# plt.ylim(-7,2)

plt.rc('text', usetex=True)
plt.rc('font', family='serif', size=20)
plt.plot(xyz_low,low,'--D',zorder=4,linewidth=4)
plt.plot(xyz_medium,medium,'-.^',zorder=3,linewidth=4)
plt.plot(xyz_high,high,':s',zorder=2,linewidth=4)
plt.plot(xyz_okada,okada,'-',zorder=1,linewidth=4)
plt.legend(('$h=3.13\cdot 10^{-3}$', '$h=1.56\cdot 10^{-3}$', '$h=7.81\cdot 10^{-4}$','Okada'),loc=3)
plt.show()

