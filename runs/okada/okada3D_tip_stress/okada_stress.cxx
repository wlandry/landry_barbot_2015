#include <iostream>
#include <Okada.hxx>

int main()
{
  const double dx=0.05/64;
  FTensor::Tensor1<double,3> origin(-.2001,-.2001,0), coord(-.2,-.2+dx/2,dx/2);
  Okada okada(1,1,-10,0,0.25,0.5,36.8698976459,53.1301023541,0,origin);

  // double dy=dx;
  // for (int i=0; i<10; ++i)
  for (int i=-84; i<100; ++i)
    {
      // coord(1)=-0.2+dy/2;
      // coord(2)=dy/2;
      // dy/=2;
      coord(0)=(i+0.5)*dx - 0.2;
      auto displacement=okada.displacement(coord);
      FTensor::Index<'i',3> j;
      double stress=2*displacement.second(0,0) + displacement.second(j,j);
      std::cout << stress << "\n";
      // std::cout
      //   // << "(" << coord(0) << " "
      //   //         << coord(1) << " "
      //   //         << coord(2) << ") "
      //   //         << displacement.first(0) << " "
      //           // << displacement.first(1) << " "
      //           // << displacement.first(2) << " "
      //           << displacement.second(0,0) << " "
      //           // << displacement.second(0,1) << " "
      //           // << displacement.second(0,2) << " "
      //           // << displacement.second(1,1) << " "
      //           // << displacement.second(1,2) << " "
      //           // << displacement.second(2,2) << " "
      //           << "\n";
    }
}
