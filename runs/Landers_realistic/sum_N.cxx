#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>

int main()
{
  std::vector<int64_t> N;
  for (int i=0; i<12; ++i)
    {
      std::stringstream ss;
      ss << "N_" << std::setw(2) << std::setfill('0') << i;
      std::ifstream infile (ss.str());
      int64_t n=0;
      int64_t nn;
      infile >> nn;
      while (infile)
        {
          n+=nn;
          infile >> nn;
        }
      N.push_back(n);
    }
  for (auto &n: N)
    std::cout << n << "\n";
}
