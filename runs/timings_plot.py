import matplotlib.pyplot as plot

plot.ylabel('Time to solve (s)')
plot.xlabel('Resolution (m)')
plot.semilogy()
plot.semilogx()

Tohoku=loadtxt("Tohoku/Tohoku.timing")
plot.plot(1000*Tohoku[:,0]/Tohoku[:,1],Tohoku[:,2],'-+',markersize=10)


Landers=loadtxt("Landers/Landers.timing")

plot.plot(1000*Landers[:,0]/Landers[:,1],Landers[:,2],':o',markersize=10)


ElMayor=loadtxt("ElMayor/ElMayor_16.timing")
plot.plot(1000*ElMayor[:,0]/ElMayor[:,1],ElMayor[:,2],'--D',markersize=10)


plot.legend(['Tohoku','Landers','El Mayor'])

plot.show()
