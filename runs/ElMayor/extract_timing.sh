grep TOTAL ElMayor_3d_mic_15.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Phi_15.timing
grep TOTAL ElMayor_3d_mic_30.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Phi_30.timing
grep TOTAL ElMayor_3d_mic_60.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Phi_60.timing
grep TOTAL ElMayor_3d_mic_120.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Phi_120.timing


grep TOTAL ElMayor_3d_1.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Xeon_1.timing
grep TOTAL ElMayor_3d_4.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Xeon_4.timing
grep TOTAL ElMayor_3d_16.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_Xeon_16.timing


grep TOTAL ElMayor_BGQ_1x16.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_BGQ_16.timing
grep TOTAL ElMayor_BGQ_4x16.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_BGQ_64.timing
grep TOTAL ElMayor_BGQ_16x16.log | cut -f 2 -d ':' | perl -pi -e "s/ *//" > ElMayor_BGQ_256.timing

