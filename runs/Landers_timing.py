import matplotlib.pyplot as plot
import numpy

plot.ylabel('Time to solve (s)')
plot.xlabel('Resolution (m)')
plot.semilogy()
plot.semilogx()
# plot.xlim(1,100000)
# plot.ylim(0.01,100000)


grid_size=300*1000
num_points=numpy.asarray([16,32,64,128,256,512,1024,2048,4096,8192,16384])
physical_size=grid_size/num_points

def sum_vector(vector):
    result=vector
    for i in range(1,len(result)):
        result[i]=result[i]+result[i-1]
    return result

Landers_1=sum_vector(numpy.loadtxt("Landers_realistic/Landers_1.timing"))
plot.plot(physical_size[:len(Landers_1)],Landers_1/16,
          ':D',markersize=10)

Landers_4=sum_vector(numpy.loadtxt("Landers_realistic/Landers_4.timing"))
plot.plot(physical_size[:len(Landers_4)],Landers_4/4,
          '--o',markersize=10)

Landers_16=sum_vector(numpy.loadtxt("Landers_realistic/Landers_16.timing"))
plot.plot(physical_size[:len(Landers_16)],Landers_16,
          '-^',markersize=10)

plot.legend(['1 core (/16)', '4 cores (/4)', '16 cores'],
            loc='top right')

plot.show()
