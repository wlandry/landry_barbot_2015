import matplotlib.pyplot as plot

plot.ylabel('Time to solve (s)')
plot.xlabel('Resolution (m)')
plot.semilogy()
plot.semilogx()
# plot.xlim(1,100000)
# plot.ylim(0.01,100000)


grid_size=360*1000
num_points=numpy.asarray([16,32,64,128,256,512,1024,2048,4096,8192])
physical_size=grid_size/num_points

def sum_vector(vector):
    result=vector
    for i in range(1,len(result)):
        result[i]=result[i]+result[i-1]
    return result

# ElMayor_Xeon_1=sum_vector(loadtxt("ElMayor/ElMayor_Xeon_1.timing"))
# plot.plot(physical_size[:len(ElMayor_Xeon_1)],ElMayor_Xeon_1/16,
#           ':D',markersize=10)

# ElMayor_Xeon_4=sum_vector(loadtxt("ElMayor/ElMayor_Xeon_4.timing"))
# plot.plot(physical_size[:len(ElMayor_Xeon_4)],ElMayor_Xeon_4/4,
#           '--D',markersize=10)

# ElMayor_Xeon_16=sum_vector(loadtxt("ElMayor/ElMayor_Xeon_16.timing"))
# plot.plot(physical_size[:len(ElMayor_Xeon_16)],ElMayor_Xeon_16,
#           '-D',markersize=10)


# ElMayor_Phi_15=sum_vector(loadtxt("ElMayor/ElMayor_Phi_15.timing"))
# plot.plot(physical_size[:len(ElMayor_Phi_15)],ElMayor_Phi_15/4,
#           ':o',markersize=10)

# ElMayor_Phi_30=sum_vector(loadtxt("ElMayor/ElMayor_Phi_30.timing"))
# plot.plot(physical_size[:len(ElMayor_Phi_30)],ElMayor_Phi_30/2,
#           '--o',markersize=10)

# ElMayor_Phi_60=sum_vector(loadtxt("ElMayor/ElMayor_Phi_60.timing"))
# plot.plot(physical_size[:len(ElMayor_Phi_60)],ElMayor_Phi_60,
#           '-o',markersize=10)


# plot.legend(['Xeon 1 core (/16)','Xeon 4 cores (/4)','Xeon 16 cores',
#              'Phi 15 cores (/4)', 'Phi 30 cores (/2)', 'Phi 60 cores'],
#             loc='lower left')

ElMayor_BGQ_16=sum_vector(loadtxt("ElMayor/ElMayor_BGQ_16.timing"))
plot.plot(physical_size[:len(ElMayor_BGQ_16)],ElMayor_BGQ_16/16,
          ':^',markersize=10)

ElMayor_BGQ_64=sum_vector(loadtxt("ElMayor/ElMayor_BGQ_64.timing"))
plot.plot(physical_size[:len(ElMayor_BGQ_64)],ElMayor_BGQ_64/4,
          '--^',markersize=10)

ElMayor_BGQ_256=sum_vector(loadtxt("ElMayor/ElMayor_BGQ_256.timing"))
plot.plot(physical_size[:len(ElMayor_BGQ_256)],ElMayor_BGQ_256,
          '-^',markersize=10)

plot.legend(['BG/Q 16 cores (/16)', 'BG/Q 64 cores (/4)', 'BG/Q 256 cores'],
            loc='lower left')

plot.show()
